# encoding: utf-8
"""
Ce fichier contient les messages utilisés par Wantzel.
Il les recharge à chaque fois qu'on lui fournit la commande '~reload' sur IRC.
"""

messages = {
    "hello":
    """Bonjour, je suis le bot de la Quadrature du Net, vous pouvez me demander de l'aide si besoin. (wantzel help)""",

    "help":
    """Mes commandes sont : ~help ~rp(cpa) ~status ~kill ~stats et ~admin.
    Pour plus d'informations, voir ici: https://wiki.laquadrature.net/Wantzel
    Pour obtenir de l'aide sur une commande en particulier, il suffit de taper ~help <commande>""",

    "help_help":
    """Bravo!
    Tu viens d'entrer dans le monde récursif où l'aide sert à expliciter l'aide.""",

    "help_rp":
    """Cette commande sert à ajouter un article à la Revue de Presse (https://wiki.laquadrature.net/Revue_de_presse)
    L'utilisation se fait sous la forme: ~rp <url de l'article à ajouter>""",

    "help_status":
    """Cette commande sert à retrouver les informations concernant un article ajouté à la Revue de Presse (https://wiki.laquadrature.net/Revue_de_presse)
    L'utilisation se fait sous la forme: ~status <url de l'article>""",

    "help_stats":
    """Cette commande permet de fournir quelques statistiques sur la Revue de Presse (https://wiki.laquadrature.net/Revue_de_presse)
    Les statistiques sont calculées sur des notes supérieurs ou égales à 0, 3, et 4. Et sur les 1, 3, 7, et 15 derniers jours.""",

    "help_kill":
    """*Attention* seuls les vrais rp-jedis ont accès à cette commande <3
    Fixe la note de l'article donné en paramètre à -100.
    Utile en cas d'erreur ou pour s'assurer que l'article ne sera pas publié dans la RP
    Utilisation: ~kill <url de l'article>""",

    "help_admin":
    """*Attention* seuls les vrais rp-jedis ont accès à cette commande <3
    Permet de gérer la liste des utilisateurs ayant un accès privilégié. Il n'y a qu'un seul niveau de privilège.
    Utilisations:
    ~admin list => Fournit la liste des utilisateurs privilégiés
    ~admin add user[, user]> => Ajoute un ou plusieurs utilisateurs à la liste
    ~admin del user[, user] => Supprime un ou plusieurs utilisateurs de la liste
    ~admin timer => Relance un timer pour gérer le topic et les tweets""",

    "rp_http":
    """Merci %s, mais je prends en compte uniquement les adresses internet qui commencent par http ou https""",

    "status_unknown_article":
    """Désolé %s, l'url donnée n'existe pas dans la base de données.""",

    "kill_none":
    """%s n'existe pas dans la base de données""",

    "kill_done":
    """%s mis à -100""",

    "stats_bravo":
    """Bravo les neurones, rien en retard depuis ces %s derniers jours!""",

    "title":
    """Titre: %s (à %s)""",

    "admin_list":
    """Liste des modérateurs actuels: %s""",

    "admin_add":
    """%s a(ont) été ajouté(s) à la liste des modérateurs""",

    "admin_add_empty":
    """Ces utilisateurs sont déjà modérateurs""",

    "admin_del":
    """%s a(ont) été retiré(s) de la liste des modérateurs.""",

    "not_moderator":
    """Désolé, il ne semble pas que vous ayez les droits pour cette commande.""",

    "topic":
    """Canal de la revue de presse de La Quadrature du Net ~ %s articles en attente ~ Mode d'emploi https://wiki.laquadrature.net/Revue_de_presse ~ Une arme, le savoir est. Le diffuser, notre devoir c'est.""",

    "tweet_rp_fr":
    """[fr] %s — %s""",

    "tweet_rp_en":
    """[en] %s — %s""",

    "new_starter":
    """Afin d'éviter des commandes prises en compte par plusieurs bots, il est désormais fortement conseillé d'utiliser des commandes commençant par '~', merci.""",

    "please_op":
    """%s j'ai besoin d'être op pour modifier le topic, ne m'oubliez pas s'il vous plait !""",

    "oped":
    """Merci %s <3""",

    "deoped":
    """Vilain %s )o;""",

    "wiki_update":
    """%s a mis à jour la page "%s" sur le wiki (%s)""",

    "coin_deltree":
    """%s ~ %s ~ Deltree ~ %s ~ %s""",

    "new_rp_new_article":
    """Merci %s, cette url a été ajoutée à la revue de presse ! À présent, que dirais-tu d'aider à choisir les extraits de l'article à publier sur #lqdn-rp ? <3""",

    "new_rp_known_article":
    """Merci %s ! Un point a été ajouté à cet article : à partir de 3, il pourra être repris dans la revue de presse. D'ailleurs, que dirais-tu d'aider à choisir les extraits à publier sur #lqdn-rp ? <3""",

    "new_rp_taken_article":
    """Merci %s ! Un point a été ajouté à cet article : il va être repris dans la revue de presse. D'ailleurs, que dirais-tu d'aider à choisir les extraits à publier sur #lqdn-rp ? <3""",

    "known_rp_new_article":
    """Merci %s, cette url a été ajoutée à la revue de presse !""",

    "known_rp_known_article":
    """Merci %s ! Un point a été ajouté à cet article : à partir de 3, il pourra être repris dans la revue de presse.""",

    "known_rp_taken_article":
    """Merci %s ! Un point a été ajouté à cet article : il va être repris dans la revue de presse.""",

    "master_rp_new_article":
    """Merci pour l'info %s !""",

    "master_rp_known_article":
    """Merci %s !""",

    "master_rp_taken_article":
    """Merci %s ! L'article est prêt pour la revue de presse !""",


}
