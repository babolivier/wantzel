-- Run with : sqlite3 db.sqlite3 -init create_database.sql

CREATE TABLE moderator(name varchar(40));
INSERT INTO moderator VALUES ('Mindiell');

CREATE TABLE tweets (last_entry_published varchar(40));
INSERT INTO tweets VALUES ('2015-01-01 00:00:00 UTC');

CREATE TABLE wikis (last_entry_updated varchar(40));
INSERT INTO wikis VALUES ('2015-01-01 00:00:00 UTC');

CREATE TABLE rpator (name varchar(50), score int);

