# encoding: utf-8
"""
Bot Wantzel from La Quadrature du Net.

License : AGPLv3
Doc     : https://wiki.laquadrature.net/Wantzel
"""

import random
import re
import sqlite3
import time

import feedparser
from irc import IrcClientFactory
import MySQLdb
from twisted.internet import reactor
from twitter import Twitter, OAuth

import config
from messages import messages

LOG_FILE = "wantzel.log"
DEBUG = 3
WARNING = 2
INFO = 1
ERROR = 0
LOG_LEVEL = DEBUG

MASTER_SCORE = 5
MASTER_CLEANING = 7  # in days

RP_CHANNEL = "#lqdn-rp"

animals = [
    {
        "left": """><((('>""",
        "right": """<')))><""",
        "sound": "blub",
    },
    {
        "left": """=^..^=""",
        "right": """=^..^=""",
        "sound": "meow",
    },
    {
        "left": """ˁ˚ᴥ˚ˀ""",
        "right": """ˁ˚ᴥ˚ˀ""",
        "sound": "wouf",
    },
    {
        "left": """\_o<""",
        "right": """>o_/""",
        "sound": "coin",
    },
    {
        "left": """^(*(oo)*)^""",
        "right": """^(*(oo)*)^""",
        "sound": "grouïk",
    },
    {
        "left": """~~(__^·>""",
        "right": """<·^__)~~""",
        "sound": "yiik",
    },
]

class Utils(object):
    """
    Simple utility class to log easily.
    """
    @classmethod
    def log(cls, message):
        """
        Logging message with timestamp.
        """
        actual_time = time.strftime("%Y-%m-%d %H:%M:%S", time.localtime())
        with open(LOG_FILE, 'a') as file_handle:
            try:
                file_handle.write("%s: %s\n" % (actual_time, message.encode("utf-8")))
            except UnicodeDecodeError:
                file_handle.write("%s: %s\n" % (actual_time, message))

    @classmethod
    def debug(cls, message):
        """
        Manage DEBUG level of logging.
        """
        if LOG_LEVEL >= DEBUG:
            cls.log("%s: %s" % ("DEBUG", message))

    @classmethod
    def warning(cls, message):
        """
        Manage WARNING level of logging.
        """
        if LOG_LEVEL >= WARNING:
            cls.log("%s: %s" % ("WARNING", message))

    @classmethod
    def info(cls, message):
        """
        Manage INFO level of logging.
        """
        if LOG_LEVEL >= INFO:
            cls.log("%s: %s" % ("INFO", message))

    @classmethod
    def error(cls, message):
        """
        Manage ERROR level of logging.
        """
        if LOG_LEVEL >= ERROR:
            cls.log("%s: %s" % ("ERROR", message))



def get_cursor():
    """
    This function connects to a MySQL database and returns a usable cursor.
    """
    connection = MySQLdb.connect(
        host=config.dbserver,
        user=config.dbuser,
        passwd=config.dbpassword,
        db=config.dbname
    )
    if connection:
        return connection.cursor()
    return None


def get_url(message):
    """
    Retrieve the url in the message.
    """
    # Let's get the url
    result = re.search("(https?[^ ]+)", message)
    if not result:
        return
    url = result.group(1)
    # Removing anchor if needed
    result = re.search("^([^#]*)", url)
    if result:
        url = result.group(1)
    # Removing trackers
    url = re.sub("[?&](utm_medium|utm_source|utm_campaign|xtor)=[^&]*", "", url)
    return url


def is_moderator(name):
    """
    This function verify if a user is a moderator.
    """
    connection = sqlite3.connect(config.sqlite_db)
    cursor = connection.cursor()
    cursor.execute("SELECT count(*) FROM moderator WHERE name=?", (name, ))
    if int(cursor.fetchone()[0]) == 1:
        return True
    return False


def tweet(message):
    """
    Tweet message on specified account
    """
    Utils.debug("tweet method")
    auth = OAuth(
        config.TOKEN,
        config.TOKENSEC,
        config.CONSKEY,
        config.CONSSEC
    )
    twitter = Twitter(auth=auth)
    try:
        Utils.debug("Tweeting: %s" % message)
        twitter.statuses.update(status=message)
    except Exception:
        pass


class Wantzel(object):
    """
    Wantzel bot.
    """
    def __init__(self):
        """
        Initialization of bot over IRC.
        """
        # Date of next cleaning
        next_week = time.localtime(time.mktime(time.localtime())+(MASTER_CLEANING*86400))
        self.next_cleaning = time.strptime("%s-%s-%s %s" % (
            next_week.tm_year,
            next_week.tm_mon,
            next_week.tm_mday,
            time.tzname[0]
        ), "%Y-%m-%d %Z")
        # Sequence for op_mode verification (fibonacci)
        self.op_sequence = [1, 2, 3, 5, 8, 13, 21, 34, 55]
        self.op_offset = 0
        self.op_counter = 0
        # Number of articles actually waiting
        self.number = None
        # default last_entry_published for tweets
        self.last_entry_published = time.strptime("2000-01-01", "%Y-%m-%d")
        # See if there is a later last_entry_published for tweets
        connection = sqlite3.connect(config.sqlite_db)
        for row in connection.execute("SELECT last_entry_published FROM tweets"):
            self.last_entry_published = time.strptime(
                row[0].encode("utf-8"),
                "%Y-%m-%d %H:%M:%S %Z"
            )
        Utils.debug("Dernier tweet: %s" % self.last_entry_published)
        # default last_entry_published for wiki
        self.last_entry_updated = time.strptime("2000-01-01", "%Y-%m-%d")
        # See if there is a later last_entry_published for wiki
        connection = sqlite3.connect(config.sqlite_db)
        for row in connection.execute("SELECT last_entry_updated FROM wikis"):
            self.last_entry_updated = time.strptime(
                row[0].encode("utf-8"),
                "%Y-%m-%d %H:%M:%S %Z"
            )
        Utils.debug("Dernière mise à jour du wiki: %s" % self.last_entry_updated)

        # Connection to IRC
        self.irc = IrcClientFactory(config)
        self.irc.set_privmsg = self.set_privmsg
        reactor.connectTCP(config.server, config.port, self.irc)
        # Prepare timer
        reactor.callLater(config.timer, self.timer)

    def timer(self):
        """
        This method launches function regularly (see config.timer).
        """
        Utils.debug("Timer called")
        # Tweet some RP based on french and english feeds
        self.rp_to_twitter("http://www.laquadrature.net/fr/revue-de-presse/feed")
        self.rp_to_twitter("http://www.laquadrature.net/en/press-review/feed")
        # Update topice based on number of articles waiting in queue 
        self.count_articles()
        # Verify op mode
        self.op_verification()
        # Tell on channel if wiki was modified since last time
        self.wiki_updates()
        # Cleaning points of mastering rp
        if time.localtime()>self.next_cleaning:
            self.clean_master_rp()
        # Recalling the timer
        reactor.callLater(config.timer, self.timer)

    def set_privmsg(self):
        """
        This method set the methods to call for each callback received from IRC.
        """
        # When receiving a message
        self.irc.client.privmsg = self.on_privmsg
        # When the bot discover it is no more op
        self.irc.client.irc_unknown = self.irc_unknown
        # When bot mode is changed
        self.irc.client.modeChanged = self.mode_changed

    def mode_changed(self, user, channel, flag_set, modes, args):
        """
        Callback called whenever bot mode is changed.
        """
        Utils.debug("Mode changed : %s %s %s %s %s" % (user, channel, flag_set, modes, args))
        if "o" in modes and self.irc.client.nickname in args:
            # Cleaning user name
            user = re.search("([^!]*)!", user).group(1)
            if flag_set:
                # thanks to user
                self.send_message(channel, messages["oped"] % user)
                # reset counter and sequence
                self.op_counter = 0
                self.op_offset = 0
            else:
                # bad user ;o(
                self.send_message(channel, messages["deoped"] % user)

    def op_verification(self):
        """
        Verify if wantzel is chan operator, if not it complains on the chan <3.
        """
        Utils.debug("op_verification method")
        # Bot tries to list channel users, if it can't, then it is no more an op
        self.irc.client.sendLine("NAMES %s" % RP_CHANNEL)

    def irc_unknown(self, prefix, command, params):
        """
        This Callback is called whenever the bot tries to perform the command "NAMES" on the
        RP_CHANNEL channel. If it occurs, then the bot knows that it is no more op and beg for
        a mode change to actual operators on the channel.
        """
        Utils.debug("UNKNOWN %s %s %s" % (prefix, command, params))
        if command=="RPL_NAMREPLY":
            # Beg for operator mode
            self.names(params)

    def names(self, params):
        """
        Send a message on channel RP_CHANNEL to beg an op mode to each actual operators.
        params is an array with :
            - params[0]:
            - params[1]:
            - params[2]: the channel
            - params[3]: the list of all users on the channel
        """
        Utils.debug("Names : %s" % params)
        if params[2]==RP_CHANNEL:
            ops = [user[1:] for user in params[3].split() if user[0]=="@"]
            if "@"+self.irc.client.nickname not in params[3]:
                # Testing based on fibonacci sequence
                self.op_counter += 1
                Utils.debug("op_counter  : %s" % self.op_counter)
                Utils.debug("op_offset   : %s" % self.op_offset)
                Utils.debug("op_sequence : %s" % self.op_sequence[self.op_offset])
                if self.op_counter>self.op_sequence[self.op_offset]:
                    self.send_message(params[2], messages["please_op"] % ", ".join(ops))
                    # Then reset op_counter
                    self.op_counter = 0
                    # And move the sequence further in order not to spam channel
                    if self.op_offset<len(self.op_sequence)-1:
                        self.op_offset += 1

    def send_message(self, channel, multiline_message):
        """
        Sends a message on specified channel, cutting each line in a new message
        """
        for message in multiline_message.splitlines():
            self.irc.client.msg(channel, message)

    def on_privmsg(self, user, channel, msg):
        """
        Wantzel can understand a lot of commands. Commands followed by a (*)
        are accessible only to moderators:
        - help
            Returns a message about how to use the bot.
            If a command is passed after help, the message explains how to use
            the command.
            Bot answers in private in order not to spam channel
        - rp(acp) <url>
            Add an article in the database with a specific flag
        - status <url>
            Retrieve some informations about an article in the database
        - stats
            Show some statistics about the RP
        - kill (*)
            Kill an article by giving it a score of -100
        - admin list (*)
            List rights in private
        - admin add (*)
            Add one or more new moderator to list
        - admin del (*)
            Delete one or more moderator from list
        - admin timer
            Relaunch a timer
        """
        # Cleaning user name
        user = re.search("([^!]*)!", user).group(1)
        Utils.debug("Message received: %s %s %s" % (user, channel, msg))
        # Never answer to botself
        if user!=config.nickname:
            # If it's a query, bot should answer to the user as the channel
            if "#" not in channel:
                channel = user
            # Help command, specific
            if "wantzel" in msg and ("help" in msg or "aide" in msg):
                self.help(user, channel, msg)
            # Find known command
            command = re.search("[!~](rp[acp]*|status|kill|help|stats|admin)", msg)
            Utils.debug("Command: %s" % command)
            if command:
                Utils.debug("group(0): %s" % command.group(0))
                command = command.group(1)
                Utils.debug("Command: %s" % command)
                if command.startswith("rp"):
                    Utils.debug("Calling self.rp")
                    self.rp(command, user, channel, msg)
                if command.startswith("status"):
                    Utils.debug("Calling self.status")
                    self.status(command, user, channel, msg)
                elif command == "help":
                    Utils.debug("Calling self.help")
                    self.help(user, channel, msg)
                elif command == "kill":
                    Utils.debug("Calling self.kill")
                    self.kill(user, channel, msg)
                elif command == "stats":
                    Utils.debug("Calling self.stats")
                    self.stats(channel)
                elif command == "admin":
                    Utils.debug("Calling self.admin")
                    self.admin(user, channel, msg)
            # Very specific command for fun only. You can easily garbage or
            # comment the next few lines
            if user.lower()=="deltree" and msg=="\_o< ~ Coin ~ >o_/":
                animal = random.choice(animals)
                self.send_message(channel, messages["coin_deltree"] % (
                    animal["left"],
                    animal["sound"],
                    animal["sound"],
                    animal["right"],
                ))
            # End of fun command.

        # No more giving the title of an url
        #if title and website:
        #    self.send_message(channel, messages["title"] % (title, website))

    def help(self, user, channel, msg):
        """
        Show global help.
        If a known command is behind the ~!help command, an adequate message is
        returned.
        """
        Utils.debug("help command")
        # Searching for a command after help keyword
        command = re.search("[!~]help (help|rp|status|stats|kill|admin)", msg)
        if command:
            command = command.group(1)
            self.send_message(user, messages["help_"+command])
        else:
            self.send_message(user, messages["help"])

    def rp(self, command, user, channel, msg):
        """
        Adding the article in rp database.
        """
        Utils.debug("rp command : %s" % command)
        Utils.debug("rp user    : %s" % user)
        Utils.debug("rp channel : %s" % channel)
        Utils.debug("rp msg     : %s" % msg)
        cite = 0
        note = 1
        answer = False
        url = get_url(msg)
        Utils.debug("url: %s" % url)
        if not url:
            return

        # Managing flags
        # LQdN is quoted
        if "c" in command:
            cite += 1
        # the article speak about LQdN
        if command.count("p") > 1:
            cite += 2
        # Archive this article
        if "a" in command:
            cite += 4

        # Looking for such an article in database
        cursor = get_cursor()
        # We need to be able to retrieve an url with "http" or "https"
        if url.startswith("https"):
            url2 = "http" + url[5:]
        else:
            url2 = "https" + url[4:]
        cursor.execute("""
            SELECT id, note, provenance
            FROM presse
            WHERE url = %s
            OR url = %s""",
            (url, url2)
        )
        rows = cursor.fetchall()
        if not rows:
            Utils.debug("Adding an article by %s: %s" % (user, url))
            cursor.execute("""
                INSERT INTO presse SET
                url=%s, provenance=%s, cite=%s, note=%s, datec=NOW(), title='',
                lang='', published=0, nid=0, screenshot=0, fetched=0, seemscite=0
                """,
                (url, user, cite, note)
            )
            answer = True
        else:
            if rows[0][2] != user:
                Utils.debug("Adding a point by %s on %s" % (user, rows[0][0]))
                cursor.execute(
                    "UPDATE presse SET note=note+1 WHERE id=%s",
                    (rows[0][0], )
                )
            note = rows[0][1]+1
            answer = True
            if note>=3:
                # Update number of articles to do
                self.count_articles()
        if answer:
            # Answer is now based on where, who, note, and a little magic
            self.did_rp(channel, user, note)

    def did_rp(self, channel, user, note):
        """
        Answers after a "rp" command has been submitted. The answer is based on the channel, the
        user and the note of the article.
        """
        know_rp = False
        known_user = False
        master_user = False
        # Retrieve user's score in database
        connection = sqlite3.connect(config.sqlite_db)
        score = 0
        for row in connection.execute("SELECT score FROM rpator WHERE name='%s'" % user):
            known_user = True
            score = int(row[0])
            if score>MASTER_SCORE:
                master_user = True
        # Channel is RP_CHANNEL, so the user is knowing the rp yet
        if channel==RP_CHANNEL:
            know_rp = True
        Utils.debug("know_rp     : %s" % know_rp)
        Utils.debug("known_user  : %s" % known_user)
        Utils.debug("score       : %s" % score)
        Utils.debug("master_user : %s" % master_user)
        # Store new score for this user
        if known_user:
            connection.execute("UPDATE rpator SET score=score+1 WHERE name='%s';" % user)
        else:
            connection.execute("INSERT INTO rpator (name, score) VALUES ('%s', 1);" % user)
        connection.commit()

        # Ok, we got all information we need, let's answer now !
        if master_user:
            # user is a rp master
            if note==1:
                self.send_message(channel, messages["master_rp_new_article"] % user)
            elif note<3:
                self.send_message(channel, messages["master_rp_known_article"] % user)
            else:
                self.send_message(channel, messages["master_rp_taken_article"] % user)
        elif known_user or know_rp:
            # user is known but not a master and/or he knows the rp channel
            if note==1:
                self.send_message(channel, messages["known_rp_new_article"] % user)
            elif note<3:
                self.send_message(channel, messages["known_rp_known_article"] % user)
            else:
                self.send_message(channel, messages["known_rp_taken_article"] % user)
        else:
            # user is unknown, and he does'nt seems to know the rp channel
            if note==1:
                self.send_message(channel, messages["new_rp_new_article"] % user)
            elif note<3:
                self.send_message(channel, messages["new_rp_known_article"] % user)
            else:
                self.send_message(channel, messages["new_rp_taken_article"] % user)

    def clean_master_rp(self):
        """
        This method cleans the known user from rp_mastering each 7 days
        """
        # Cleaning users
        connection = sqlite3.connect(config.sqlite_db)
        connection.execute("UPDATE rpator SET score=score-1 WHERE user='%s'" % user)
        connection.execute("DELETE FROM rpator WHERE score<1")
        connection.commit()
        # Date of next cleaning
        next_week = time.localtime(time.mktime(time.localtime())+(MASTER_CLEANING*86400))
        self.next_cleaning = time.strptime("%s-%s-%s %s" % (
            next_week.tm_year,
            next_week.tm_mon,
            next_week.tm_mday,
            time.tzname[0]
        ), "%Y-%m-%d %Z")

    def status(self, command, user, channel, msg):
        """
        Retrieving status of the article in rp database.
        """
        Utils.debug("rp command : %s" % command)
        Utils.debug("rp user : %s" % user)
        Utils.debug("rp channel : %s" % channel)
        Utils.debug("rp msg : %s" % msg)
        url = get_url(msg)
        Utils.debug("url: %s" % url)
        if not url:
            return

        # Looking for such an article in database
        cursor = get_cursor()
        # We need to be able to retrieve an url with "http" or "https"
        if url.startswith("https"):
            url2 = "http" + url[5:]
        else:
            url2 = "https" + url[4:]
        cursor.execute("""
            SELECT cite, nid, note
            FROM presse
            WHERE url = %s
            OR url = %s""",
            (url, url2)
        )
        rows = cursor.fetchall()
        if not rows:
            self.send_message(channel, messages["status_unknown_article"] % user)
        else:
            message = "%s: note %s / " % (user, rows[0][2])
            if rows[0][0] & 1:
                message += "cite LQdN / "
            if rows[0][0] & 2:
                message += "parle de LQdN / "
            if rows[0][0] & 4:
                message += "archivé / "
            if rows[0][1] > 0:
                message += "publié (https://laquadrature.net/node/%s) / " % rows[0][1]
            else:
                message += "non publié / "
            self.send_message(channel, message[:-3])

    def kill(self, user, channel, msg):
        """
        Kill an article by setting its score to -100.
        """
        Utils.debug("kill command")
        if is_moderator(user):
            url = get_url(msg)
            Utils.debug("url: %s" % url)
            if url == "":
                return
            elif url == "http":
                self.send_message(channel, messages["rp_http"] % user)
                return
            # Looking for such an article in database
            cursor = get_cursor()
            # We need to be able to retrieve an url with "http" or "https"
            if url.startswith("https"):
                url2 = "http" + url[5:]
            else:
                url2 = "https" + url[4:]
            cursor.execute("""
                SELECT id, note
                FROM presse
                WHERE url = %s
                OR url = %s""",
                (url, url2)
            )
            rows = cursor.fetchall()
            if not rows:
                self.send_message(channel, messages["kill_none"] % url)
            else:
                cursor.execute("UPDATE presse SET note=-100 WHERE id=%s", (rows[0][0], ))
                self.send_message(channel, messages["kill_done"] % url)
        else:
            self.send_message(channel, messages["not_moderator"])

    def stats(self, channel):
        """
        Returns stats on articles in press review.
        """
        Utils.debug("stats command")
        cursor = get_cursor()
        periods = [1, 3, 7, 15]
        notes = [0, 3, 4]
        notnull = 0
        somethingatall = 0
        for note in notes:
            notnull = 0
            period_result = ""
            for period in periods:
                cursor.execute("""
                    SELECT COUNT(id) AS cid
                    FROM presse
                    WHERE nid=0
                    AND datec>(NOW()-INTERVAL %s DAY)
                    AND note>=%s""",
                    (period, note)
                )
                rows = cursor.fetchall()
                if rows[0][0] > 0:
                    period_result = period_result + "%sj:%s, " % (period, rows[0][0])
                    notnull = 1
                    somethingatall = 1
            if notnull:
                self.send_message(channel, "note>=%s: " % note + period_result[:-2])
        if somethingatall == 0:
            self.send_message(channel, messages["stats_bravo"] % periods[-1])

    def admin(self, user, channel, msg):
        """
        Manage moderation.
        A sub-command should be behind the !~admin command.
        """
        Utils.debug("admin command")
        # Searching for a command after admin keyword
        command = re.search("[~!]admin (list|add|del|timer)", msg)
        if command:
            command = command.group(1)
            if command == "list":
                self.admin_list(user, channel)
            elif command == "add":
                self.admin_add(user, channel, msg)
            elif command == "del":
                self.admin_del(user, channel, msg)
            elif command == "timer":
                self.admin_timer(user, channel)

    def admin_list(self, user, channel):
        """
        List actual moderators.
        """
        Utils.debug("admin_list command")
        if is_moderator(user):
            connection = sqlite3.connect(config.sqlite_db)
            names = []
            for row in connection.execute("SELECT name FROM moderator"):
                names.append(row[0].encode("utf-8"))
            self.send_message(channel, messages["admin_list"] % ", ".join(sorted(names)))
        else:
            self.send_message(channel, messages["not_moderator"])

    def admin_add(self, user, channel, msg):
        """
        Add some new moderators if not existing yet.
        """
        Utils.debug("admin_add command")
        if is_moderator(user):
            try:
                names = []
                connection = sqlite3.connect(config.sqlite_db)
                result = re.search("[!~]admin add (([^,]+, ?)+)?(.*)", msg)
                if result.group(1):
                    names = [name.strip() for name in result.group(1).split(",") if name.strip() != ""]
                names.append(result.group(3))
                # Do not add actual moderators
                moderators = []
                for row in connection.execute("SELECT name FROM moderator"):
                    moderators.append(row[0].encode("utf-8"))
                names = list(set([name for name in names if name not in moderators]))
                if names:
                    # Converting set in list of tuples
                    values = [(name,) for name in names]
                    connection.executemany("INSERT INTO moderator (name) VALUES (?)", values)
                    connection.commit()
                    self.send_message(channel, messages["admin_add"] % ", ".join(names))
                else:
                    self.send_message(channel, messages["admin_add_empty"])
            except Exception:
                pass
        else:
            self.send_message(channel, messages["not_moderator"])

    def admin_del(self, user, channel, msg):
        """
        Delete a moderator from list.
        """
        Utils.debug("admin_del command")
        if is_moderator(user):
            try:
                names = []
                result = re.search("[!~]admin del (([^,]+, ?)+)?(.*)", msg)
                if result.group(1):
                    names = [name.strip() for name in result.group(1).split(",") if name.strip() != ""]
                names.append(result.group(3))
                names = list(set(names))
                Utils.debug(names)
                connection = sqlite3.connect(config.sqlite_db)
                for name in names:
                    connection.execute("DELETE FROM moderator WHERE name=?", (name, ))
                connection.commit()
                self.send_message(channel, messages["admin_del"] % ", ".join(names))
            except Exception:
                pass
        else:
            self.send_message(channel, messages["not_moderator"])

    def admin_timer(self, user, channel):
        """
        Relaunch a timer.
        """
        Utils.debug("admin_timer command")
        if is_moderator(user):
            try:
                # Recalling the timer
                reactor.callLater(config.timer, self.timer)
            except Exception:
                pass
        else:
            self.send_message(channel, messages["not_moderator"])

    def count_articles(self):
        """
        Count number of articles not done in RP and updates the topic of the
        press review channel if needed.
        """
        Utils.debug("count_articles method")
        cursor = get_cursor()
        cursor.execute("""SELECT COUNT(*) FROM presse
            WHERE DATE_SUB(NOW(), INTERVAL 2 MONTH)<datec
            AND note > 2
            AND nid = 0""")
        rows = cursor.fetchall()
        number = int(rows[0][0])
        Utils.debug("Found %s articles." % number)
        if self.number != number:
            self.irc.client.topic(RP_CHANNEL, messages["topic"] % number)
        self.number = number

    def rp_to_twitter(self, rss):
        """
        By parsing the RSS feed of the press-review, we know what to tweet.
        """
        Utils.debug("rp_to_twitter method")
        now = time.localtime()
        today = time.strptime("%s-%s-%s %s" % (
            now.tm_year,
            now.tm_mon,
            now.tm_mday,
            time.tzname[0]
            ), "%Y-%m-%d %Z")
        language = "fr"
        if "/en/" in rss:
            language = "en"
        entries = feedparser.parse(rss)['entries']
        entries.reverse()
        Utils.debug(self.last_entry_published)
        for entry in entries:
            # if date of publication is greater than today, midnight, and
            # lesser than future
            if today < entry.published_parsed < now:
                if self.last_entry_published < entry.published_parsed:
                    # Let's see if we can truncate the lenght of the tweet
                    # We have 5 chars for the language, so max-length is 135
                    title = entry.title.encode("utf-8")
                    link = entry.link.encode("utf-8")
                    if len(title) + min(len(link),23) > 135:
                        # What is the number of chars we need to remove
                        excess = len(title) + min(len(link),23) - 135
                        title = ''.join([title[:-(excess + 4)], ' ...'])
                    tweet(messages["tweet_rp_%s" % language] % (
                        title,
                        link
                    ))
                    Utils.debug(entry.published_parsed)
                    Utils.debug(entry.title)
                    # Save last_entry_published
                    self.last_entry_published = entry.published_parsed
                    last_entry_published = time.strftime(
                        "%Y-%m-%d %H:%M:%S %Z",
                        self.last_entry_published
                    )
                    connection = sqlite3.connect(config.sqlite_db)
                    connection.execute(
                        "UPDATE tweets SET last_entry_published=?",
                        (last_entry_published,)
                    )
                    connection.commit()
                    # Tweet only one message in order not to spam
                    return
                else:
                    Utils.debug(entry.title)
                    Utils.debug(entry.published_parsed)

    def wiki_updates(self):
        url = "https://wiki.laquadrature.net/api.php?days=1&limit=50&translations=filter&action=feedrecentchanges&feedformat=atom"
        now = time.localtime()
        today = time.strptime("%s-%s-%s %s" % (
            now.tm_year,
            now.tm_mon,
            now.tm_mday,
            time.tzname[0]
            ), "%Y-%m-%d %Z")
        entries = feedparser.parse(url)['entries']
        for entry in entries:
            # if date of update is greater than today midnight
            if today < entry.updated_parsed:
                if self.last_entry_updated < entry.updated_parsed:
                    # Ecriture de la mise à jour sur le canal de travail
                    self.send_message("#lqdn-travail", messages["wiki_update"] % (
                        entry.author.encode("utf-8"),
                        entry.title.encode("utf-8"),
                        entry.link.encode("utf-8"),
                    ))
                    # Save last_entry_published
                    self.last_entry_updated = entry.updated_parsed
                    last_entry_updated = time.strftime(
                        "%Y-%m-%d %H:%M:%S %Z",
                        self.last_entry_updated
                    )
                    connection = sqlite3.connect(config.sqlite_db)
                    connection.execute(
                        "UPDATE wikis SET last_entry_updated=?",
                        (last_entry_updated,)
                    )
                    connection.commit()


if __name__ == '__main__':
    Wantzel()
    reactor.run()
