-- Run in a running instance of mysql

CREATE TABLE IF NOT EXISTS `presse` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `url` varchar(255) NOT NULL,
  `cite` tinyint(3) unsigned NOT NULL,
  `lang` varchar(2) NOT NULL,
  `published` tinyint(3) unsigned NOT NULL,
  `datec` datetime NOT NULL,
  `nid` int(10) unsigned NOT NULL,
  `screenshot` tinyint(3) unsigned NOT NULL,
  `title` varchar(255) NOT NULL,
  `fetched` tinyint(4) NOT NULL,
  `seemscite` tinyint(4) NOT NULL,
  `provenance` varchar(128) NOT NULL,
  `note` tinyint(4) NOT NULL DEFAULT '0' COMMENT 'Note (urgence)',
  `date_publi` datetime DEFAULT NULL,
  `hat` text,
  `media` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `note` (`note`)
) ENGINE=MyISAM AUTO_INCREMENT=24233 DEFAULT CHARSET=latin1
