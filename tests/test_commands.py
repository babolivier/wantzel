# encoding: utf-8
"""
Testing commands of wantzel
"""

from mock import Mock
import unittest
import sqlite3

import config
from messages import messages
from wantzel import DEBUG, get_cursor, Utils, Wantzel

LOG_FILE = "tests/wantzel.log"
LOG_LEVEL = DEBUG

config.dbuser      = "root"
config.dbpassword  = "root"
config.dbserver    = "localhost"
config.dbname      = "wantzel_test"
config.sqlite_db   = "tests/db_test.sqlite3"


class TestWantzel(unittest.TestCase):
    def setUp(self):
        self.wantzel = Wantzel()
        # Faking send_message on IRC
        self.wantzel.send_message = Mock()
        # Faking topic change on IRC
        self.wantzel.irc = Mock()
        self.wantzel.irc.client = Mock()
        self.wantzel.irc.client.topic = Mock()
        cursor = get_cursor()
        # clearing datas
        cursor.execute("DELETE FROM presse")
        connection = sqlite3.connect(config.sqlite_db)
        connection.execute("DELETE FROM moderator")
        connection.execute("INSERT INTO moderator (name) VALUES ('admin')")
        connection.commit()

    def tearDown(self):
        # Destroying database
        cursor = get_cursor()
        cursor.execute("DELETE FROM presse")
        connection = sqlite3.connect(config.sqlite_db)
        connection.execute("DELETE FROM moderator")
        connection.commit()

    def testHelpAnswerIsInQuery(self):
        self.wantzel.on_privmsg("test!test.me", "#test_channel", "~help")
        self.wantzel.send_message.assert_called_once_with("test", messages["help"])

    def testSubHelpAnswersAreInQuery(self):
        commands = ['help', 'rp', 'status', 'stats', 'kill', 'admin']
        for command in commands:
            self.wantzel.on_privmsg("test!test.me", "#test_channel", "~help %s" % command)
            self.wantzel.send_message.assert_called_with("test", messages["help_%s" % command])

    def testRpCommandWithNoUrlDoesNothing(self):
        self.wantzel.on_privmsg("test!test.me", "#test_channel", "~rp nothing")
        self.wantzel.send_message.assert_not_called()
        cursor = get_cursor()
        cursor.execute("SELECT COUNT(*) FROM presse")
        rows = cursor.fetchall()
        self.assertEqual(0, int(rows[0][0]))

    def testRpCommandWithNewArticleSavesIt(self):
        self.wantzel.on_privmsg("test!test.me", "#test_channel", "~rp http://test.me/article")
        self.wantzel.send_message.assert_called_once_with(
            "#test_channel",
            messages["rp_new_article"] % "test"
        )
        cursor = get_cursor()
        cursor.execute("SELECT COUNT(*) FROM presse")
        rows = cursor.fetchall()
        self.assertEqual(1, int(rows[0][0]))
        cursor.execute("SELECT note FROM presse WHERE url='%s'" % "http://test.me/article")
        rows = cursor.fetchall()
        self.assertEqual(1, int(rows[0][0]))

    def testDoubleRpCommandWithAKnownArticleIsNotModifyingNote(self):
        cursor = get_cursor()
        # Adding article for first time
        self.wantzel.on_privmsg("test!test.me", "#test_channel", "~rp http://test.me/article")
        # Then, just add a point
        self.wantzel.on_privmsg("test!test.me", "#test_channel", "~rp http://test.me/article")
        self.wantzel.send_message.assert_called_with(
            "#test_channel",
            messages["rp_known_article"] % "test"
        )
        cursor.execute("SELECT COUNT(*) FROM presse")
        rows = cursor.fetchall()
        self.assertEqual(1, int(rows[0][0]))
        cursor.execute("SELECT note FROM presse WHERE url='%s'" % "http://test.me/article")
        rows = cursor.fetchall()
        self.assertEqual(1, int(rows[0][0]))

    def testRpCommandWithAKnownArticle(self):
        cursor = get_cursor()
        # Adding article for first time
        self.wantzel.on_privmsg("first_test!test.me", "#test_channel", "~rp http://test.me/article")
        # Then, just add a point
        self.wantzel.on_privmsg("test!test.me", "#test_channel", "~rp http://test.me/article")
        self.wantzel.send_message.assert_called_with(
            "#test_channel",
            messages["rp_known_article"] % "test"
        )
        cursor.execute("SELECT COUNT(*) FROM presse")
        rows = cursor.fetchall()
        self.assertEqual(1, int(rows[0][0]))
        cursor.execute("SELECT note FROM presse WHERE url='%s'" % "http://test.me/article")
        rows = cursor.fetchall()
        self.assertEqual(2, int(rows[0][0]))

    def testRpCommandWithAPublishableArticle(self):
        cursor = get_cursor()
        # Adding article for first time
        self.wantzel.on_privmsg("first_test!test.me", "#test_channel", "~rp http://test.me/article")
        self.wantzel.on_privmsg("second_test!test.me", "#test_channel", "~rp http://test.me/article")
        # Then, add a third point
        self.wantzel.on_privmsg("test!test.me", "#test_channel", "~rp http://test.me/article")
        self.wantzel.send_message.assert_called_with(
            "#test_channel",
            messages["rp_taken_article"] % "test"
        )
        self.wantzel.irc.client.topic.assert_called_once_with("#lqdn-rp", messages["topic"] % 1)
        cursor.execute("SELECT COUNT(*) FROM presse")
        rows = cursor.fetchall()
        self.assertEqual(1, int(rows[0][0]))
        cursor.execute("SELECT note FROM presse WHERE url='%s'" % "http://test.me/article")
        rows = cursor.fetchall()
        self.assertEqual(3, int(rows[0][0]))

    def testStatusCommandForAnUnknownArticle(self):
        self.wantzel.on_privmsg("test!test.me", "#test_channel", "~status http://test.me/article")
        self.wantzel.send_message.assert_called_with(
            "#test_channel",
            messages["status_unknown_article"] % "test"
        )

    def testStatusCommandForAKnownArticle(self):
        self.wantzel.on_privmsg("test!test.me", "#test_channel", "~rp http://test.me/article")
        for i in range(1, 5):
            self.wantzel.on_privmsg("test!test.me", "#test_channel", "~status http://test.me/article")
            self.wantzel.send_message.assert_called_with(
                "#test_channel",
                "test: note %s / non publié" % i
            )
            self.wantzel.on_privmsg("other_test!test.me", "#test_channel", "~rp http://test.me/article")

    def testStatusCommandForAPublishedArticle(self):
        cursor = get_cursor()
        self.wantzel.on_privmsg("test!test.me", "#test_channel", "~rp http://test.me/article")
        self.wantzel.on_privmsg("test!test.me", "#test_channel", "~status http://test.me/article")
        self.wantzel.send_message.assert_called_with(
            "#test_channel",
            "test: note 1 / non publié"
        )
        for i in range(1, 5):
            cursor.execute("UPDATE presse SET nid=%s WHERE url='%s'" % (
                i,
                "http://test.me/article",
            ))
            self.wantzel.on_privmsg("test!test.me", "#test_channel", "~status http://test.me/article")
            self.wantzel.send_message.assert_called_with(
                "#test_channel",
                "test: note 1 / publié (https://laquadrature.net/node/%s)" % i
            )

    def testStatsCommandWithNoArticle(self):
        self.wantzel.on_privmsg("test!test.me", "#test_channel", "~stats")
        self.wantzel.send_message.assert_called_with(
            "#test_channel",
            messages["stats_bravo"] % 15
        )

    def testStatsCommandWithOneArticle(self):
        self.wantzel.on_privmsg("test!test.me", "#test_channel", "~rp http://test.me/article")
        self.wantzel.on_privmsg("test!test.me", "#test_channel", "~stats")
        self.wantzel.send_message.assert_called_with(
            "#test_channel",
            "note>=0: 1j:1, 3j:1, 7j:1, 15j:1"
        )

    def testStatsCommandWithSomeOldArticles(self):
        cursor = get_cursor()
        # Old article
        self.wantzel.on_privmsg("test!test.me", "#test_channel", "~rp http://test.me/old_article")
        cursor.execute("""
            UPDATE presse
            SET datec=(NOW()-INTERVAL 5 DAY)
            WHERE url='%s'""" % "http://test.me/old_article"
        )
        # New article
        self.wantzel.on_privmsg("test!test.me", "#test_channel", "~rp http://test.me/new_article")
        self.wantzel.on_privmsg("test!test.me", "#test_channel", "~stats")
        self.wantzel.send_message.assert_called_with(
            "#test_channel",
            "note>=0: 1j:1, 3j:1, 7j:2, 15j:2"
        )

    def testKillCommandWithoutCredentials(self):
        self.wantzel.on_privmsg("test!test.me", "#test_channel", "~kill http://test.me/article")
        self.wantzel.send_message.assert_called_with(
            "#test_channel",
            messages["not_moderator"]
        )

    def testKillCommandWithAnUnknownArticle(self):
        self.wantzel.on_privmsg("admin!test.me", "#test_channel", "~kill http://test.me/unknown_article")
        self.wantzel.send_message.assert_called_with(
            "#test_channel",
            messages["kill_none"] % "http://test.me/unknown_article"
        )

    def testKillCommandWithAKnownArticle(self):
        self.wantzel.on_privmsg("admin!test.me", "#test_channel", "~rp http://test.me/article")
        self.wantzel.on_privmsg("admin!test.me", "#test_channel", "~kill http://test.me/article")
        self.wantzel.send_message.assert_called_with(
            "#test_channel",
            messages["kill_done"] % "http://test.me/article"
        )
        cursor = get_cursor()
        cursor.execute("SELECT note FROM presse WHERE url='%s'" % "http://test.me/article")
        rows = cursor.fetchall()
        self.assertEqual(-100, int(rows[0][0]))

    def testAdminListCommandWithoutCredentials(self):
        self.wantzel.on_privmsg("test!test.me", "#test_channel", "~admin list")
        self.wantzel.send_message.assert_called_with(
            "#test_channel",
            messages["not_moderator"]
        )

    def testAdminListCommand(self):
        self.wantzel.on_privmsg("admin!test.me", "#test_channel", "~admin list")
        self.wantzel.send_message.assert_called_with(
            "#test_channel",
            messages["admin_list"] % "admin"
        )

    def testAdminAddCommandWithoutCredentials(self):
        self.wantzel.on_privmsg("test!test.me", "#test_channel", "~admin add test")
        self.wantzel.send_message.assert_called_with(
            "#test_channel",
            messages["not_moderator"]
        )

    def testAdminAddCommandWithOneName(self):
        self.wantzel.on_privmsg("admin!test.me", "#test_channel", "~admin add toto")
        self.wantzel.on_privmsg("admin!test.me", "#test_channel", "~admin list")
        self.wantzel.send_message.assert_called_with(
            "#test_channel",
            messages["admin_list"] % "admin, toto"
        )

    def testAdminAddCommandWithMultipleName(self):
        self.wantzel.on_privmsg("admin!test.me", "#test_channel", "~admin add tata, titi,tutu, toto")
        self.wantzel.on_privmsg("admin!test.me", "#test_channel", "~admin list")
        self.wantzel.send_message.assert_called_with(
            "#test_channel",
            messages["admin_list"] % "admin, tata, titi, toto, tutu"
        )

    def testAdminDelCommandWithoutCredentials(self):
        self.wantzel.on_privmsg("test!test.me", "#test_channel", "~admin del test")
        self.wantzel.send_message.assert_called_with(
            "#test_channel",
            messages["not_moderator"]
        )

    def testAdminDelCommandWithOneName(self):
        self.wantzel.on_privmsg("admin!test.me", "#test_channel", "~admin add tata, titi,tutu, toto")
        self.wantzel.on_privmsg("admin!test.me", "#test_channel", "~admin list")
        self.wantzel.send_message.assert_called_with(
            "#test_channel",
            messages["admin_list"] % "admin, tata, titi, toto, tutu"
        )
        self.wantzel.on_privmsg("admin!test.me", "#test_channel", "~admin del titi")
        self.wantzel.on_privmsg("admin!test.me", "#test_channel", "~admin list")
        self.wantzel.send_message.assert_called_with(
            "#test_channel",
            messages["admin_list"] % "admin, tata, toto, tutu"
        )

    def testAdminDelCommandWithMultipleName(self):
        self.wantzel.on_privmsg("admin!test.me", "#test_channel", "~admin add tata, titi,tutu, toto")
        self.wantzel.on_privmsg("admin!test.me", "#test_channel", "~admin list")
        self.wantzel.send_message.assert_called_with(
            "#test_channel",
            messages["admin_list"] % "admin, tata, titi, toto, tutu"
        )
        self.wantzel.on_privmsg("admin!test.me", "#test_channel", "~admin del toto, tutu,titi")
        self.wantzel.on_privmsg("admin!test.me", "#test_channel", "~admin list")
        self.wantzel.send_message.assert_called_with(
            "#test_channel",
            messages["admin_list"] % "admin, tata"
        )



    def notest(self):
        # Adding article for first time
        self.wantzel.on_privmsg("first_test!test.me", "#test_channel", "~rp http://test.me/article")
        self.wantzel.on_privmsg("second_test!test.me", "#test_channel", "~rp http://test.me/article")
        # Then, test its status
        self.wantzel.on_privmsg("test!test.me", "#test_channel", "~rp http://test.me/article")
        self.wantzel.send_message.assert_called_with(
            "#test_channel",
            messages["rp_taken_article"] % "test"
        )
        self.wantzel.irc.client.topic.assert_called_once_with("#lqdn-rp", messages["topic"] % 1)
        cursor.execute("SELECT COUNT(*) FROM presse")
        rows = cursor.fetchall()
        self.assertEqual(1, int(rows[0][0]))
        cursor.execute("SELECT note FROM presse WHERE url='%s'" % "http://test.me/article")
        rows = cursor.fetchall()
        self.assertEqual(3, int(rows[0][0]))

